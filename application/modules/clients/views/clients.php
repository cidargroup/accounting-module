<ul class="breadcrumb">
	<li><a href="<?php echo base_url();?>dashboard" class="glyphicons home"><i></i>Verge</a></li>
	<li class="divider"></li>
	<li>Clients</li>
</ul>
<div class="separator"></div>

<div class="heading-buttons">
	<h3 class="glyphicons shopping_cart"><i></i> Manage Clients/Customers</h3>
	<div class="buttons pull-right">
		<a href="<?php echo base_url();?>clients/add" class="btn btn-primary btn-icon glyphicons circle_plus"><i></i> Add Customer</a>
		
	</div>
</div>
<div class="separator"></div>
<?php 
$roleid=$this->session->userdata('roleid');
?>
<div class="innerLR">
</div>

<div class="widget widget-2" style="margin: 0;">
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i></i> Manage Clients/Customers</h4>
	</div>

</div>

<br/>
<?php 
if(!isset($info)):?>
<div class='alert alert-error'>
<strong>Error!</strong> You have not created any Clients yet for your business.
</div>
<?php else : ?>
<table class="table table-bordered table-condensed table-striped table-vertical-center checkboxs js-table-sortable table-thead-simple table-thead-border-none">
	<thead>
		<tr>
			<th>Name</th>
			<th class="center">Address</th>
			<th class="center">Telephone</th>
			<?php if($roleid==1):?>
	        <th class='center' style='width:100px;'>Actions</th>
	        <?php endif;?>
		</tr>
	</thead>
	<tbody>
		<?php foreach($info as $info) :?>
		<tr class='selectable'>
			<td class='important'><a href='<?php echo base_url();?>clients/view/<?php echo $info['id'];?>'><?php echo $info['name'];?></a></td>
			<td class='center js-sortable-handle'><?php echo $info['address'];?></td>
			<td class='center'><?php echo $info['telephone'];?></td>
		    <?php if($roleid==1):?>
			<td class='center'>
				<a href='<?php echo base_url();?>clients/edit/<?php echo $info['id'];?>' class='btn-action glyphicons pencil btn-success'><i></i></a>
				<a href='<?php echo base_url();?>clients/history/<?php echo $info['id'];?>' class='btn-action glyphicons justify btn-info'><i></i></a>
				<a href='<?php echo base_url();?>clients/delete/<?php echo $info['id'];?>' class='btn-action glyphicons remove_2 btn-danger'><i></i></a>
			</td>
			<?php endif;?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<br/>
<?php endif ;?>	
		</div>
		</div>	
	</div>