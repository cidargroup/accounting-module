<ul class="breadcrumb">
	<li><a href="<?php echo base_url();?>dashboard" class="glyphicons home"><i></i>Verge</a></li>
	<li class="divider"></li>
	<li>Client Information</li>
</ul>
<div class="separator"></div>

<div class="heading-buttons">
	<h3 class="glyphicons shopping_cart"><i></i><?php echo $info['name'];?> Information</h3>
	
</div>

<div class="tab-pane active" id="productDescriptionTab">
<div class="row-fluid">
    <div class="span3">
            <strong>Client Name:</strong>

    </div>
    <div class="span9">
    <?php echo$info['name'];?>
    </div></div>
<div class="row-fluid">
    <div class="span3">
            <strong>Client Address:</strong>

    </div>
    <div class="span9">
    <?php echo $info['address'];?>
    </div></div>
<div class="row-fluid">
    <div class="span3">
            <strong>Client Telephone:</strong>

    </div>
    <div class="span9">
    <?php echo $info['telephone'];?>
    </div></div>
<div class="row-fluid">
    <div class="span3">
            <strong>Client Email:</strong>

    </div>
    <div class="span9">
    <?php echo $info['email'];?>
    </div></div>
                </div>
<div class="separator"></div>

<div class="innerLR">
</div>
<div class="widget widget-2" style="margin: 0;">
	<div class="widget-head">
		<h4 class="heading glyphicons list"><i></i> <?php echo $info['name'];?> Purchase History</h4>
	</div>

</div>

<br/>
<?php
if(!isset($record)):?>
<p>This customer currently does not have a purchase history.</p>
<?php else : ?>
<table class="table table-bordered table-condensed table-striped table-vertical-center checkboxs js-table-sortable table-thead-simple table-thead-border-none">
	<thead>
	<tr>
	<th>Outlet Name</th>
	<th class="center">Payment Method</th>
	<th class="center">Amount</th>
	<th class="center">Date</th>

	</tr>
	</thead>
	<tbody>
	<?php foreach($record as $record) :?>
	<?php
	echo("<tr class='selectable'>");?>
	<td class='important'><?php echo $record['name'];?></td>
	<td class='center js-sortable-handle'><?php echo $record['paymethod'];?></td>
	<td class='center'><?php echo $record['amount'];?></td>
	<td class='center'><?php echo $record['created'];?></td>

<?php echo("</tr>"); ?>
<?php endforeach; ?>
</tbody>
</table>
<br/>
<?php endif ;?>
		
		
		</div>
		</div>	
	</div>
	
