<?php
Class Clients_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}

public function getall($firmid){
	$this->db->select()->from('clients')->where('firm_id',$firmid);
	$result=$this->db->get();
		if($result->num_rows()>0){
		return $result->result_array();
		}
	}
public function getclient($clientID,$firmid){

	
	$this->db->select()->from('clients');
	$this->db->where('id',$clientID);
	$this->db->where('firm_id',$firmid);

	$result=$this->db->get();
	if($result->num_rows()>0){
		return $result->first_row('array');
		}
	}

	public function addclient($data)
	{
		$data['created'] = date('Y-m-d H:i:s', time());
		$this->db->insert('clients', $data);
		return $this->db->insert_id();
	}

public function updateclient($clientID,$firmid,$data){
	$this->db->update('clients',$data,array('id' => $clientID,'firm_id'=>$firmid));
	}

public function deleteclient($clientID){
	$this->db->where('id',$clientID);
	$this->db->delete('clients');
	
	}

public function searchclient($clientname,$firmid){
	
	$this->db->select()->from('clients');
	$this->db->like('name',$clientname);
	$this->db->where('firm_id',$firmid);
	
	$result=$this->db->get();
	return $result->result_array();
		
	}
}


?>