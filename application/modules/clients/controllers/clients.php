<?php
 class Clients extends CI_Controller{
    function __construct(){
        parent::__construct();
        $this->load->model('clients_model');
    }

function index(){
	    if(! $this->session->userdata('userid')){
            redirect('login');
        }
     $firmid=$this->session->userdata('firmid');
    $result['info']=$this->clients_model->getall($firmid);
    $this->load->view('templates/innerheader');
    $this->load->view('clients',$result);
    $this->load->view('templates/innerfooter');
	}

function view($clientID){
	 $firmid=$this->session->userdata('firmid');
	 $result['info']=$this->clients_model->getclient($clientID,$firmid);
	 $this->load->model('receipts_model');
	 $result['record']=$this->receipts_model->getclient_transaction($clientID);

	 $this->load->view('templates/innerheader');
	 $this->load->view('clientinfo',$result);
	 $this->load->view('templates/innerfooter');
	}

function add($msg=NULL){
	$data['msg']=$msg;
	$this->load->view('templates/innerheader');
	$this->load->view('addclients',$data);
	$this->load->view('templates/innerfooter');
	}

function insert(){
	if($_POST){
		$status=$this->session->userdata('status');
	if($status=="ACTIVE"){
		
	$this->load->helper('date');
	$firmid=$this->session->userdata('firmid');
		$data=array(
			'name'=>$_POST['name'],
			'email'=>$_POST['email'],
			'telephone'=>$_POST['telephone'],
			'firm_id'=>$firmid,
			'address'=>$_POST['address'],
			'created'=>date('Y-m-d H:i:s',now())
			);
		$this->clients_model->addclient($data);
		redirect('clients');
		}else{
	$msg="<p class='error'>Your subscription is either PENDING OR EXPIRED and you cannot add a new client without an ACTIVE subscription</p>";
	$data['msg']=$msg;
	$this->load->view('templates/innerheader');
	$this->load->view('addclients',$data);
	$this->load->view('templates/innerfooter');
		}

	}else{
			redirect('clients');
		}
	}

function edit($clientID){
	$firmid=$this->session->userdata('firmid');
	$result['info']=$this->clients_model->getclient($clientID,$firmid);

	$this->load->view('templates/innerheader');
	$this->load->view('editclient',$result);
	$this->load->view('templates/innerfooter');
	}

function update($clientID){
	if($_POST){
	$firmid=$this->session->userdata('firmid');
	$data=array(
		'name'=>$_POST['name'],
		'email'=>$_POST['email'],
		'telephone'=>$_POST['telephone'],
		'address'=>$_POST['address']
		);
	$this->clients_model->updateclient($clientID,$firmid,$data);

	redirect('clients');
		}else{
			redirect('clients');
		}
	}

function delete($clientID){
	$this->clients_model->deleteclient($clientID);
	redirect('clients');
}

}


?>
