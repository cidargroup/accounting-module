<?php
Class Selectplan_model extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	public function getplan($packageid)
	{
		return $this->db->get_where('packages', array('id' => $packageid))->row();
	}
}