<?php
Class Firm extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	public function create($data)
	{
		$this->db->insert('firms', $data);
		return $this->db->insert_id();
	}

public function newsubscription($info){
	$this->db->insert('subscriptions',$info);
	return $this->db->insert_id();
}

public function superadmin($data){
	$this->db->insert('users', $data);
}


public function getinfo($firmid){
	$this->db->select()->from('firms')->where('id',$firmid);
	$result=$this->db->get();
	return $result->first_row('array');

	}

public function updateinfo($firmid,$data){
	$this->db->update('firms',$data,array('id' => $firmid));
}

}


?>