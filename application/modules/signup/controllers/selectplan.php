<?php
Class Selectplan extends CI_Controller{
	function __construct(){
	parent::__construct();
	}

public function index(){
	$this->load->view('templates/header');
	$this->load->view('selectplan');
	$this->load->view('templates/footer');
}

public function process(){
	$package=$this->input->post('package');
	$duration=$this->input->post('duration');
	$pack = explode("_", $package);
	$packageid=$pack[0];
	$price = (int)($duration * $pack[1]);
	//store duration and packageid in session for future queries
	$this->session->set_userdata('duration',$duration);
	$this->session->set_userdata('packageid',$packageid);
	// get package name from model
	$this->load->model('selectplan_model');
	$package=$this->selectplan_model->getplan($packageid);

	$data['price']=$price;
	$data['packagename']=$package->name;
	$data['duration']=$duration;

	$this->load->view('templates/header');
	$this->load->view('checkplan',$data);
	$this->load->view('templates/footer');

}


public function review(){
	//allow user to complete the signup form to create their account
	//$paymethod['paymethod']=$this->input->post('paymethod');
	$paymethod['paymethod']="offline";
	$this->load->view('templates/header');
	$this->load->view('signup',$paymethod);
	$this->load->view('templates/footer');
}


}
?>