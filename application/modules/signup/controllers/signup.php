<?php
Class Signup extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->load->model(array('role_model'));
	}

public function index(){
	//check whether user can access this page or redirect user to selectplan.php
	$this->load->view('templates/header');
	$this->load->view('selectplan');
	$this->load->view('templates/footer');
}

public function account(){
	//insert company information and redirect user to create admin account. send subscription email
	if($_POST)
	{
		$this->load->helper('date');
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'gif|jpg|png';
		//$config['max_size']	= '100';
		$config['max_width']  = '1024';
		$config['max_height']  = '768';
		$this->load->library('upload', $config);
		$field_name = "img";

		//if ( ! $this->upload->do_upload($field_name))
		if (FALSE)
		{
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('templates/header');
			$this->load->view('signup', $error);
			$this->load->view('templates/footer');
		}
		else
		{
			$image_path = $this->upload->data('img');
			$path=$image_path['file_name'];
			$data=array(
			'name'=>$_POST['bizname'],
			'address'=>$_POST['address'],
			'telephone'=>$_POST['telephone'],
			'email'=>$_POST['email'],
			'state'=>$_POST['state'],
			'logo'=>$path,
			'created'=>date('Y-m-d H:i:s',now()),
			'country'=>$_POST['country']
			);

			//determine default subscription status
			if($_POST['paymethod']=='offline'){
			$status="PENDING";
			}else{
			$status="ACTIVE";
			}

			//create company account
			$this->load->model('firm');
			$firm_id = $this->firm->create($data);
			$role_id = $this->role_model->insert(array('firm_id' => $firm_id, 'name' => 'Busines Owner', 'slug' => 'business-owner', 'description' => 'Business Owner', 'status' => 1));
			$this->session->set_userdata('firmid', $firm_id);

			// create subscription information for the firm
			//check plan expiration date

			//$duration=$this->session->userdata('duration');
			$duration=1;

			if($duration==1)
			{
				$expiredate=date("Y-m-d H:i:s", strtotime("+30 days",time()));
			}
			elseif ($duration==3)
			{
				$expiredate=date("Y-m-d H:i:s", strtotime("+90 days",time()));
			}
			elseif ($duration==6)
			{
				$expiredate=date("Y-m-d H:i:s", strtotime("+180 days",time()));
			}
			elseif ($duration==12)
			{
				$expiredate=date("Y-m-d H:i:s", strtotime("+360 days",time()));
			}
			elseif ($duration==24)
			{
				$expiredate=date("Y-m-d H:i:s", strtotime("+720 days",time()));
			}

			$info=array(
				'package_id'=>$this->session->userdata('packageid'),
				'firm_id'=>$firm_id,
				'status'=>$status,
				'startdate'=>date('Y-m-d H:i:s',now()),
				'enddate'=>$expiredate
			);
			$subscription=$this->firm->newsubscription($info);
			//send welcome email to email specified in the form
			/*if($_POST['email'])
			{
				$email=$_POST['email'];
				$this->load->library('email',$config);
				$this->email->from('info@verge.com.ng','Verge');
				$this->email->to($email);
				$this->email->subject('Welcome to Verge');
				$this->email->message("Verge is the leading Business Intelligence solution across Africa and we are glad to have you
				using our solution, our commitment is simple: to enable you gain rich insight and control into your business operations, process and customers.
				We believe we can achieve this with you and are glad you have choosen to use our solution.
				subscription Status:$status\n
				Expiration Date:$expiredate\n
				If you need any help working you through using the solution or having any suggestions or thoughts, please feel free to reach us through any
				of the following mediums:\n
				Email: support@verge.com.ng\n
				Phone: 2348077708099, 2347064332692\n");
				$this->email->send();
			}*/
			//load the create admin view
			$this->load->view('templates/header');
			$this->load->view('masteradmin');
			$this->load->view('templates/footer');
		}
	}
	else
	{
		$this->load->view('templates/header');
		$this->load->view('selectplan');
		$this->load->view('templates/footer');
	}
}


	public function admin(){
		//create admin account for the company and send welcome email then redirect user to login view
		if($_POST)
		{
			$roleid=1;
			$userstatus="ACTIVE";
			$data = array(
				'username'=>$_POST['username'],
				'password'=>sha1($_POST['password']),
				'telephone'=>$_POST['telephone'],
				'email'=>$_POST['email'],
				'user_status'=>$userstatus,
				'role_id'=>$roleid,
				'firm_id'=>$this->session->userdata('firmid')
				);

			$this->load->model('firm');
			$admin=$this->firm->superadmin($data);

			if($_POST['email'])
			{
				/*$email=$_POST['email'];
				$password=$_POST['password'];
				$username=$_POST['username'];
				
				$this->load->library('email',$config);
				$this->email->from('info@verge.com.ng','Verge');
				$this->email->to($email);
				$this->email->subject('Welcome to Verge');
				$this->email->message("Verge is the leading Business Intelligence solution across Africa and we are glad to have you
				using our solution, our commitment is simple: to enable you gain rich insight and control into your business operations, process and customers.
				<p>We believe we can achieve this with you and are glad you have choosen to use our solution.\nPlease find below your login information:\n
				Username: $username\n
				Password:<strong>$password</strong>\n");

				$this->email->send();*/
			}
			redirect('login');

		}
		else
		{
			$this->load->view('templates/header');
			$this->load->view('selectplan');
			$this->load->view('templates/footer');
		}
	}	
}

