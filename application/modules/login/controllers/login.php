<?php
Class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('login_model');
	}


	public function index($msg = NULL)
	{
		$data['msg'] = $msg;
		$this->load->view('templates/header_2');
		$this->load->view('templates/sidemenu', $data);
		$this->load->view('templates/body');
		$this->load->view('templates/footer_2');
	}

	public function process()
	{
		//validate user can login
		$username = $this->input->post('username');
		$password = sha1($this->input->post('password'));

		if(empty($username) OR empty($password))
		{
			$msg = '<p class="alert-error">Kindly provide a username and password</p>';
			$this->index($msg);	
		}
		else
		{
			$result = $this->login_model->validate($username, $password);
			if( ! $result)
			{
				$msg = '<p class="alert-error">Inactive account or invalid username and/or password</p>';
				$this->index($msg);
			}
			elseif($result['role_id'] > 1 && ! $this->login_model->checkoutlet($result['id']))
			{
				$msg = '<p class="alert-error">Sorry you have not been asssigned to any Outlet.</p>';
				$this->index($msg);
			}
			else
			{
				$this->load->helper('date');
				$data = array('last_login' => date('Y-m-d H:i:s', now()));
				$this->login_model->updaterecord($username, $data);

				$this->session->set_userdata('userid', $result['id']);
				$this->session->set_userdata('username', $result['username']);
				$this->session->set_userdata('email', $result['email']);
				$this->session->set_userdata('firmid', $result['firm_id']);
				$this->session->set_userdata('roleid', $result['role_id']);
				$this->session->set_userdata('modules', $result['modules']);

				redirect('dashboard');
			}
		}		
	}

	public function forgetpass($msg = NULL)
	{
		$data['msg'] = $msg;

		$this->load->view('templates/header');
		$this->load->view('forget', $data);
		$this->load->view('templates/footer');
	}

	public function sendpass()
	{
		$this->load->model('login_model');
		$email = $this->input->post('email');
		$result = $this->login_model->sendpassword($email);

		if( ! $result)
		{
			$msg = '<p class="alert-error">There is no account associated with the email address you entered</p>';
			$this->forgetpass($msg);
		}
		else
		{
			//send email and redirect user to success page
			//generate random password and update user account and send to user
			$unique_key = substr(md5(rand(0, 1000000)), 0, 8);
			$newpassword = sha1($unique_key);
			$theinfo = array('password' => $newpassword);
			$this->load->model('user_model');
			$this->user_model->forgetpassupdate($email, $theinfo);

			$this->load->library('email');
			$this->email->from('info@verge.com.ng','Verge');
			$this->email->to($email);
			$this->email->subject('Verge Password Recovery');
			$this->email->message("Please find below the new password to enable you access your account:

			<p>New Password: $unique_key</p>
			<p>If you need any help working you through using the solution or having any suggestions or thoughts, please feel free to reach us through any
			of the following mediums:</p>
			<p>Email: support@verge.com.ng</p>
			<p>Phone: +2348077708099, +2347064332692</p>");

			$this->email->send();
			$this->load->view('templates/header');
			$this->load->view('sendpassword');
			$this->load->view('templates/footer');
		}
	}
}
?>