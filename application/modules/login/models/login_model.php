<?php
Class Login_model extends CI_Model{

	function __construct()
	{
		parent::__construct();
	}


	public function validate($username, $password)
	{
		//grab user input
		$status = "ACTIVE";
		$param = array(
			'username'    => $username,
			'password'    => $password,
			'user_status' => $status
		);

		return $this->db
			 ->select("users.*, permissions.modules")
			 ->join('roles', "roles.id = users.role_id", 'LEFT')
			 ->join('permissions', "permissions.role_id = roles.id", 'LEFT')
		     ->where($param)
		     ->get('users')->row_array();
	}

	public function sendpassword($email)
	{
		$this->db->select()->from('users')->where('email', $email);
		$result = $this->db->get();

		return $result->first_row('array');
	}

	public function updaterecord($username, $data)
	{
		$this->db->update('users', $data, array('username' => $username));
	}

	public function checkoutlet($userid)
	{
		$result = $this->db->where('user_id', $userid)->get_where('stores_users');
		return ($result->num_rows() > 0) ? TRUE : FALSE;
	}
}
?>