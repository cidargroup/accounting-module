    <div id="login">
    <form class="form-signin" method="post" action='<?php echo base_url();?>login/process'>
        <div class="widget widget-4">
            <div class="widget-head">
                <h4 class="heading">Restricted Area</h4>
            </div>
        </div>
        <h2 class="glyphicons unlock form-signin-heading"><i></i> Sign In</h2>
        <div class="uniformjs">
            <input type="text" class="input-block-level" placeholder="Username" name="username"> 
            <input type="password" class="input-block-level" placeholder="Password" name="password"> 
            
        </div>
        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
        <a class="btn btn-large btn-primary" href='<?php echo base_url();?>selectplan'>Sign up</a>
        <p><a href="<?php echo base_url();?>login/forgetpass">Forget password</a></p>
        <br/><?php if(! is_null($msg)) echo $msg;?>  
    </form>
</div>  