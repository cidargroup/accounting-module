	<!-- Themer -->
	<script>
		var themerPrimaryColor = '#DA4C4C';
	</script>

	<script src="<?php echo base_url();?>assets/bootstrap/extend/bootstrap-wysihtml5/js/bootstrap-wysihtml5-0.0.2.js" type="text/javascript"></script>

	<script src="<?php echo base_url();?>assets/theme/scripts/load.js"></script>
	<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap-typeahead.js"></script>
	<script src="<?php echo base_url();?>assets/bootstrap/extend/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>

	<!-- Resize Script -->
	<script src="<?php echo base_url();?>assets/theme/scripts/jquery.ba-resize.js"></script>
	<!-- Bootstrap Script -->

	<script src="<?php echo base_url();?>assets/theme/scripts/jquery.cookie.js"></script>
	<script src="<?php echo base_url();?>assets/theme/scripts/themer.js"></script>

	<!-- Custom Onload Script -->

	<script src="<?php echo base_url();?>assets/theme/scripts/jquery-miniColors/jquery.miniColors.js"></script>

	<script src="<?php echo base_url();?>assets/theme/scripts/jquery-ui-1.9.2.custom/js/jquery-ui-1.9.2.custom.min.js"></script>

	<script type="text/javascript" src="https://www.google.com/jsapi"></script>

	<!-- Uniform -->
	<script src="<?php echo base_url();?>assets/theme/scripts/pixelmatrix-uniform/jquery.uniform.min.js"></script>
	
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-46962069-1', 'verge.com');
	  ga('send', 'pageview');

	</script>
</body>
</html>