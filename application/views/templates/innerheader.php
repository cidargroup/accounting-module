<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>    <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html> <!--<![endif]-->
<head>
	<title>Verge</title>
	
	<!-- Meta -->
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	
	<!-- Bootstrap -->
	<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/bootstrap/extend/jasny-bootstrap/css/jasny-bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>assets/bootstrap/extend/bootstrap-wysihtml5/css/bootstrap-wysihtml5-0.0.2.css" rel="stylesheet" />
	
	
	<!-- JQueryUI v1.9.2 -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/theme/scripts/jquery-ui-1.9.2.custom/css/smoothness/jquery-ui-1.9.2.custom.min.css" />
	
	<!-- Glyphicons -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/theme/css/glyphicons.css" />
	
	<!-- Bootstrap Extended -->
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/extend/bootstrap-select/bootstrap-select.css" />
	<link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/extend/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
	
	<!-- Uniform -->
	<link rel="stylesheet" media="screen" href="<?php echo base_url();?>assets/theme/scripts/pixelmatrix-uniform/css/uniform.default.css" />

	<!-- JQuery v1.8.2 -->
	<script src="<?php echo base_url();?>assets/theme/scripts/jquery-1.8.2.min.js"></script>
	
	<!-- Modernizr -->
	<script src="<?php echo base_url();?>assets/theme/scripts/modernizr.custom.76094.js"></script>
	
	<!-- MiniColors -->
	<link rel="stylesheet" media="screen" href="<?php echo base_url();?>assets/theme/scripts/jquery-miniColors/jquery.miniColors.css" />
	
	<!-- Theme -->
	<link rel="stylesheet/less" href="<?php echo base_url();?>assets/theme/less/style.less?1361270115" />
	
	<!-- FireBug Lite -->
	<!-- <script type="text/javascript" src="https://getfirebug.com/firebug-lite-debug.js"></script> -->
	
	<!-- LESS 2 CSS -->
	<script src="<?php echo base_url();?>assets/theme/scripts/less-1.3.3.min.js"></script>
	<script src="<?php echo base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/theme/scripts/flot/jquery.flot.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/theme/scripts/flot/jquery.flot.pie.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/theme/scripts/flot/jquery.flot.tooltip.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/theme/scripts/flot/jquery.flot.selection.js"></script>
	<script src="<?php echo base_url();?>assets/theme/scripts/flot/jquery.flot.resize.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/theme/scripts/flot/jquery.flot.orderBars.js" type="text/javascript"></script>
	<script src="<?php echo base_url();?>assets/bootstrap/js/prettify.js"></script>
	<script src="<?php echo base_url();?>assets/bootstrap/extend/bootstrap-select/bootstrap-select.min.js"></script>	
</head>
<body>
<!-- Start Content -->
	<div class="container-fluid fluid">
		
		<div class="navbar main">
			<a href="<?php echo base_url();?>" class="appbrand"><span>Verge</span></a>
			
						<button type="button" class="btn btn-navbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
			</button>
						
			<ul class="topnav pull-right">
				
				<li class="account">
										<a data-toggle="dropdown" href="#" class="glyphicons logout lock"><span class="hidden-phone text"><?php echo ucfirst($this->session->userdata('username'));?></span><i></i></a>
					<ul class="dropdown-menu pull-right">
						
						<li class="highlight profile">
							<span>
								<span class="heading">Profile <a href="<?php echo base_url();?>users/profile" class="pull-right">edit</a></span>
									<span class="details">
									<a href="<?php echo base_url();?>users/profile"><?php echo ucfirst($this->session->userdata('username'));?></a>
									<p><?php echo strtolower($this->session->userdata('email'));?></p>
									</span>
								<span class="clearfix"></span>
							</span>
						</li>
						<li>
							<span>
								<a class="btn btn-default btn-small pull-right" style="padding: 2px 10px; background: #fff;" href="<?php echo base_url();?>dashboard/do_logout">Sign Out</a>
							</span>
						</li>
					</ul>
				</li>
			</ul>
		</div>
		
				<div id="wrapper">
		<div id="menu" class="hidden-phone">
			<div id="menuInner">
<ul>
    <li class='heading'><span>Main Menu</span></li>
    <?php
    	$modules = $this->session->userdata('modules');
    	$permitted_modules =  $modules ? json_decode($modules, TRUE) : array();
    	if($this->session->userdata('roleid')==1):
    ?>
    <li class='glyphicons home'><a href='<?php echo base_url();?>dashboard'><i></i><span>Dashboard</span></a></li>
    <li class='glyphicons cogwheel'><a href='<?php echo base_url();?>setting'><i></i><span>Settings</span></a></li>
    <li class='glyphicons cogwheel'><a href='<?php echo base_url();?>roles'><i></i><span>Roles</span></a></li>
    <li class='glyphicons cogwheels'><a href='<?php echo base_url();?>outlet'><i></i><span>Outlets</span></a></li>
    <li class='glyphicons user'><a href='<?php echo base_url();?>users'><i></i><span>Users</span></a></li>
    <li class='glyphicons user_add'><a href='<?php echo base_url();?>clients'><i></i><span>Clients/Customers</span></a></li>
     <li class='glyphicons charts'><a href='<?php echo base_url();?>warehouse'><i></i><span>Warehouse</span></a></li>
    <li class='glyphicons charts'><a href='<?php echo base_url();?>products'><i></i><span>Products</span></a></li>
    <li class=''><a class='glyphicons table' href='<?php echo base_url();?>inventory'><i></i><span>Inventory</span></a>
    </li>
    <li class='glyphicons coins'><a href='<?php echo base_url();?>receipts'><i></i><span>Receipts</span></a></li>
    <li class='glyphicons history'><a href='<?php echo base_url();?>transaction'><i></i><span>Transaction History</span></a></li>
    <li class='glyphicons charts'><a href='<?php echo base_url();?>reports'><i></i><span>Reports</span></a></li>
    <?php elseif($this->session->userdata('roleid')>=2):?>
	 	<li class='glyphicons home'><a href='<?php echo base_url();?>dashboard'><i></i><span>Dashboard</span></a></li>
    <?php endif;?>
    <?php if(in_array(1, $permitted_modules)):?>
	    <li class='glyphicons cogwheels'><a href='<?php echo base_url();?>outlet'><i></i><span>Outlets</span></a></li>
    <?php endif;?>
    <?php if(in_array(5, $permitted_modules)):?>    
	    <li class='glyphicons user_add'><a href='<?php echo base_url();?>clients'><i></i><span>Clients/Customers</span></a></li>
    <?php endif;?>
    <?php if(in_array(6, $permitted_modules)):?>
	     <li class='glyphicons charts'><a href='<?php echo base_url();?>warehouse'><i></i><span>Warehouse</span></a></li>
    <?php endif;?>
    <?php if(in_array(7, $permitted_modules)):?>
	    <li class='glyphicons charts'><a href='<?php echo base_url();?>products'><i></i><span>Products</span></a></li>
    <?php endif;?>
    <?php if(in_array(8, $permitted_modules)):?>
	    <li class=''><a class='glyphicons table' href='<?php echo base_url();?>inventory'><i></i><span>Inventory</span></a></li>
    <?php endif;?>
    <?php if(in_array(9, $permitted_modules)):?>
	    <li class='glyphicons coins'><a href='<?php echo base_url();?>receipts'><i></i><span>Receipts</span></a></li>
    <?php endif;?>
    <?php if(in_array(10, $permitted_modules)):?>
	    <li class='glyphicons history'><a href='<?php echo base_url();?>transaction'><i></i><span>Transaction History</span></a></li>
    <?php endif;?>
    <?php if(in_array(11, $permitted_modules)):?>
	    <li class='glyphicons charts'><a href='<?php echo base_url();?>reports'><i></i><span>Reports</span></a></li>
	<?php endif;?>
	</ul>			
</div>
</div>
<div id="content">
		