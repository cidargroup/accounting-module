</div>
	<!-- Themer -->
	<script>
		var themerPrimaryColor = '#DA4C4C';
	</script>
		<script src="<?php echo base_url();?>assets/theme/scripts/jquery.cookie.js"></script>
		<script src="<?php echo base_url();?>assets/theme/scripts/themer.js"></script>
		<script src="<?php echo base_url();?>assets/theme/scripts/jquery-1.8.2.min.js"></script>
		<!-- Custom Onload Script -->
		<script src="<?php echo base_url();?>assets/theme/scripts/load.js"></script>
	
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-46962069-1', 'verge.com');
		ga('send', 'pageview');
	</script>
</body>
</html>